module.exports = {
  "modulePaths": [
    "<rootDir>/",
    "<rootDir>/src"
  ],
  "transformIgnorePatterns": [
    "/node_modules/"
  ],
  "collectCoverageFrom": [
    "src/**/*.{js,jsx}",
    "!src/index.js"
  ],
  "coverageThreshold": {
    "global": {
      "branches": 90,
      "functions": 90,
      "lines": 90,
      "statements": 90
    }
  },
  "moduleNameMapper": {
    "\\.(css|less|scss)$": "__mocks__/styleMock.js"
  },
  "setupFiles": [
    "<rootDir>/testSetup.js"
  ]
}