import React from 'react';

class GameContainer extends React.Component {
  render() {
    return (
      <div
        className='game-container'
      >
        {this.props.gameTiles.map((tile) => {
          let selected = tile.ID === this.props.target ? 'selected' : '';
          let empty = tile.val === null ? 'empty' : '';
          return (
            <div
              className={`tile ${selected} ${empty}`}
              onClick={() => {
                this.props.onTileClick(tile.ID, tile.val);
              }}
              key={tile.ID}
              style={{top: `${tile.pos.y * 5}rem`, left: `${tile.pos.x * 5}rem`}}
            >
              {tile.val}
            </div>)
        })}
      </div>
    );
  }
}

export default GameContainer;