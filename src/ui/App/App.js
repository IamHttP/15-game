import React from 'react';
import GameContainer from '../GameContainer/GameContainer';
import Menu from "../Menu/Menu";

class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      gameTiles: [],
      target: null,
      moves: 0,
      gameWon:false
    }
  }
  
  componentDidMount() {
    this.setupNewGame();
  }
  
  setupNewGame() {
    this.game = this.props.getGameInstance(this.props.height, this.props.width, this.props.randomStart);
  
    window.game = this.game;
    this.game.onStateChange((tiles, gameWon) => {
      this.setState({
        gameTiles: tiles,
        gameWon
      });
    });
  
    this.setState({
      gameTiles: this.game.getState(),
      target: null,
      moves: 0,
      gameWon:false
    });
  }
  
  render() {
    return (
      <div
        className='container'
      >
        {this.state.gameWon ? <h1 className='game-won'>GAME WON</h1> : null}
        <Menu
          moves={this.state.moves}
          onNewGame={() => {
            this.setupNewGame();
          }}
        />
        <GameContainer
          gameTiles={this.state.gameTiles}
          target={this.state.target}
          onTileClick={(ID, val) => {
            // set target if we don't have one set
            if (this.state.target === null) {
              // do not allow to select the empty tile
              if (val !== null) {
                this.setState({
                  target: ID
                });
              }
            } else {
              // perform the move, game will notify us of state change
              // with the listener we created in componentDidMount
              let legalMove = this.game.move(this.state.target, ID);
  
              this.setState({
                target: null,
                moves: legalMove ? this.state.moves + 1: this.state.moves
              });
            }
          }}
        ></GameContainer>
      </div>
    );
  }
}

export default App;