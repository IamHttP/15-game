import React from 'react';

class Menu extends React.Component {
  render() {
    return (
      <div className='menu'>
        <h1>15 Game</h1>
        <button
          className='btn'
          onClick={this.props.onNewGame}
        >New Game
        </button>
        <h1>Moves : {this.props.moves}</h1>
      </div>
    );
  }
}

export default Menu;