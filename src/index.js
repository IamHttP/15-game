import './styles/global.scss';
import React from 'react';
import {render} from 'react-dom';

let root = document.createElement('div');
document.body.appendChild(root);

import App from './ui/App/App.js';
import Game from './game/Game.js';

render(<App
  randomStart={true}
  height={4}
  width={4}
  getGameInstance={(h, w, isRandom) => {
    return new Game(h, w, isRandom);
  }}
/>, root);

