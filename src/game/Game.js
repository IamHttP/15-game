class Tile {
  constructor(val, ID) {
    this.val = val;
    this.ID = ID;
  }
  
  setPosition(pos) {
    this.pos = pos;
  }
}

class Game {
  constructor(h, w, randomStart = true) {
    this.height = h;
    this.width = w;
    let tileCount = (h * w);
    let i = 0;
    this.tiles = [];
    this.tileByID = {};
    this.tileByValue = {};
    
    while (i < tileCount - 1) {
      let val = i + 1; // turn index to number
      let tile = new Tile(val, i);
      
      this.tiles.push(tile);
      this.tileByID[i] = tile;
      this.tileByValue[val] = tile;
      i++;
    }
    
    let tile = new Tile(null, tileCount - 1);
    
    this.tiles.push(tile);
    this.tileByID[i] = tile;
    this.tileByValue[null] = tile;
    
    this.createBoard();
    
    if (randomStart) {
      this.shuffle();
    }
  }
  
  shuffle() {
    let i = 0;
    while (i < (this.height * this.width) * 2) {
      this.randomMove();
      i++;
    }
  }
  
  randomMove() {
    let valueIdx = this.tilesToArr();
    let nullPosInArr = valueIdx.indexOf(null);
    let nullTile = this.tileByValue[null];
    let top = this.tileByValue[valueIdx[nullPosInArr - this.width]];
    let bottom = this.tileByValue[valueIdx[nullPosInArr + this.width]];
    let left = this.tileByValue[valueIdx[nullPosInArr - 1]];
    let right = this.tileByValue[valueIdx[nullPosInArr + 1]];
  
    let moves = [top, bottom, left, right].filter((targetTile) => {
      return this.isMoveLegal(nullTile, targetTile);
    });
    
    let targetTile = moves[Math.floor(Math.random() * moves.length)];
    this.move(nullTile.ID, targetTile.ID);
  }
  
  createBoard() {
    let i = 0;
    let j = 0;
    let positions = [];
    
    while (i < this.height) {
      j = 0;
      while (j < this.width) {
        positions.push({
          x: j,
          y: i
        });
        j++;
      }
      i++;
    }
    
    positions = positions.reverse();
    
    this.tiles.forEach((tile) => {
      tile.setPosition(positions.pop());
    });
    
    return this.tiles;
  }
  
  onStateChange(fn) {
    this.listener = fn;
  }
  
  getState() {
    return this.tiles;
  }
  
  tilesToArr() {
    let arr = [];
    this.tiles.forEach((tile) => {
      let {x, y} = tile.pos;
      arr[x + y * this.width] = tile.val;
    });
    
    return arr;
  }
  
  calcWin() {
    let arr = this.tilesToArr();
    
    for (let i = 0; i < arr.length - 2; i++) {
      // if item is undefined before the end, we did not win.
      // all items, up until the last one, all follow to i = i - 1
      if (arr[i] !== arr[i+1] - 1) {
        return false;
      }
    }
    
    // if the item is not null, it's not a win
    return arr[arr.length-1] === null
  }
  
  emitState() {
    // we create a new array to send back
    this.listener && this.listener([...this.tiles], this.calcWin());
  }
  
  sameRow(t1, t2) {
    return t1.pos.y === t2.pos.y;
  }
  
  sameCol(t1, t2) {
    return t1.pos.x === t2.pos.x;
  }
  
  isMoveLegal(t1, t2) {
    if (!t1 || !t2) {
      return false;
    }
    if (t1.val !== null && t2.val !== null) {
      return false;
    }
    
    if (this.sameRow(t1, t2)) {
      return Math.abs(t1.pos.x - t2.pos.x) === 1; // dif has to be 1.
    }
    
    if (this.sameCol(t1, t2)) {
      return Math.abs(t1.pos.y - t2.pos.y) === 1; // dif has to be 1.
    }
  }
  
  move(originID, targetID) {
    // at least one has to be null, else it's illegal
    let t1 = this.tileByID[originID];
    let t2 = this.tileByID[targetID];
    
    if (!this.isMoveLegal(t1, t2)) {
      return false;
    }
    
    let targetPos = t1.pos;
    let originPos = t2.pos;
    
    t1.pos = originPos;
    t2.pos = targetPos;
    
    this.emitState();
    return true;
  }
}

export default Game;