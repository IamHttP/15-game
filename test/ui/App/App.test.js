import App from 'ui/App/App.js';
import React from 'react';
import { mount } from 'enzyme';
import Game from 'game/Game.js';
import Menu from "../../../src/ui/Menu/Menu";
import GameContainer from "../../../src/ui/GameContainer/GameContainer";


describe('Render App', () => {
  it ('Expect to render child components with correct props', () => {
    let wrapper = mount(<App
      getGameInstance={(h, w) => {
        return new Game(h, w);
      }}
    ></App>);

    let menuProps = wrapper.find(Menu).props();
    expect(menuProps.moves).toBe(0);
    expect(typeof menuProps.onNewGame).toBe('function');
    
    
    let GameContainerProps = wrapper.find(GameContainer).props();
    expect(GameContainerProps.target).toBe(null);
    expect(typeof GameContainerProps.onTileClick).toBe('function');
    expect(GameContainerProps.gameTiles.length).toBeGreaterThan(0);
    expect(GameContainerProps.gameTiles.push).toBeDefined(); // ensuring it's an array
  });
  
  it('Should change game state by UI clicks, given a valid move', () => {
    let wrapper = mount(<App
      getGameInstance={(h, w) => {
        let random = false;
        return new Game(h, w, random);
      }}
    ></App>);
    
    // since all the above code is sync, we can safely access game
    let props = wrapper.find(GameContainer).props();
    let firstPos = props.gameTiles[15].pos;
    
    expect(props.target).toBe(null);
    props.onTileClick( 14, 15); // ID/Value
    wrapper.update();
    
    props = wrapper.find(GameContainer).props();
    expect(props.target).toBe(14);


    props.onTileClick( 15, null);
    wrapper.update();
    
    props = wrapper.find(GameContainer).props();
    expect(props.target).toBe(null);

    props = wrapper.find(GameContainer).props();

    let newFirstPos = props.gameTiles[0].pos;

    expect(firstPos.y).not.toBe(newFirstPos.y);
  });
  
  it('Can set up a new game', () => {
    let wrapper = mount(<App
      getGameInstance={(h, w) => {
        return new Game(h, w);
      }}
    ></App>);
    
    // since all the above code is sync, we can safely access game
    let firstTiles = wrapper.find(GameContainer).props().gameTiles;
    wrapper.find(Menu).props().onNewGame(); // shuffle a new game
    
    // let secondTiles = wrapper.find(GameContainer).props().gameTiles;
    
    // expect(firstTiles[0].pos.x).not.toBe(secondTiles[0].pos.x);
  });
});