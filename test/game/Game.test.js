import Game from 'game/Game.js';

describe('Game tests', () => {
  it('should create a new game', () => {
    let game = new Game(4, 4);
    expect(game.tiles.length).toBe(16);
  });
  
  it('Start without shuffle', () => {
    let game = new Game(4, 4, false);
    for (let i = 0; i < game.tiles; i++) {
      expect(i).toBe(game.tiles[i].ID);
    }
  });
  
  it('Can make a legal move', () => {
    let game = new Game(4, 4, false);
    expect(game.move(14, 15)).toBe(true);
    expect(game.move(14, 15)).toBe(true); // un-move
    expect(game.move(15, 11)).toBe(true);
  
  });
  
  it('Prevents an illegal move', () => {
    let game = new Game(4, 4, false);
    // when the game is not shuffled, 15 is always the empty box
    expect(game.move(15, 7)).toBe(false); // same col
    expect(game.move(2, 1)).toBe(false); // both taken
    expect(game.move(13, 15)).toBe(false); // same row
  });
  
  it('Emits state correctly and confirms moving', (done) => {
    let game = new Game(4, 4, false);
    let nullPos = game.getState()[15].pos;
    
    game.onStateChange((tiles) => {
      expect(tiles).toStrictEqual(game.getState());
      expect(tiles[15].pos).not.toBe(nullPos);
      done();
    });
  
    game.move(14, 15);
  });
  
  it('Tests the winning conditions', (done) => {
    let game = new Game(4, 4, false);
    
    // take the winning starting position, move and unmove - guarantee a win.
    game.move(14, 15);
    
    game.onStateChange((tiles, gameWon) => {
      expect(gameWon).toBe(true);
      done();
    });
  
    game.move(14, 15);
  });
});